# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

dato = Campo.new
dato.layout_id=1
dato.name="valor1"
dato.level=0
dato.number_field=1
dato.save

dato = Campo.new
dato.layout_id=1
dato.name="valor2"
dato.level=1
dato.number_field=2
dato.save

dato = Campo.new
dato.layout_id=1
dato.name="valor3"
dato.level=1
dato.number_field=3
dato.save

dato = Campo.new
dato.layout_id=1
dato.name="valor4"
dato.level=0
dato.number_field=4
dato.save

dato = Campo.new
dato.layout_id=1
dato.name="valor5"
dato.level=0
dato.number_field=5
dato.save

dato = Campo.new
dato.layout_id=1
dato.name="valor6"
dato.level=1
dato.parent=0
dato.number_field=6
dato.save