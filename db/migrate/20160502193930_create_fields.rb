class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|

      t.integer :layout_id
      t.integer :number_field
      t.integer :fieldtype_id
      t.string :name
      t.boolean :changeable
      t.integer :length
      t.boolean :parent
      t.integer :level
      t.string :alias

      t.timestamps null: false
    end
  end
end
