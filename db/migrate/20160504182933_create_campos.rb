class CreateCampos < ActiveRecord::Migration
  def change
    create_table :campos do |t|
      t.integer :layout_id
      t.integer :number_field
      t.string :name
      t.boolean :parent
      t.integer :level

      t.timestamps null: false
    end
  end
end
