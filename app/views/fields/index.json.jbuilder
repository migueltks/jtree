json.array!(@fields) do |field|
  json.extract! field, :id, :id, :layout_id, :number_field, :fieldtype_id, :name, :changeable, :length, :parent, :level, :alias
  json.url field_url(field, format: :json)
end
