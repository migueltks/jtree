json.array!(@campos) do |campo|
  json.extract! campo, :id, :layout_id, :number_field, :name, :parent, :level
  json.url campo_url(campo, format: :json)
end
