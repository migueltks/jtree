// This is a manifest file that'll be compiled into application, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks

//= require toastr/toastr.min.js
//= require nestable/jquery.nestable.js
//= require codemirror/codemirror.js
//= require codemirror/mode/javascript/javascript.js
//= require validate/jquery.validate.min.js
//= require jsTree/jstree.min.js
//= require diff_match_patch/javascript/diff_match_patch.js
//= require preetyTextDiff/jquery.pretty-text-diff.min.js
//= require tinycon/tinycon.min.js
//= require idle-timer/idle-timer.min.js
//= require jquery-ui/jquery-ui.min.js
//= require sweetalert/sweetalert.min.js

//= require_tree .

