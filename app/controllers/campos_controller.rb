class CamposController < ApplicationController
  before_action :set_campo, only: [:show, :edit, :update, :destroy]

  # GET /campos
  # GET /campos.json
  def index
    @campos = Campo.all
  end

  # GET /campos/1
  # GET /campos/1.json
  def show
  end

  # GET /campos/new
  def new
    @campo = Campo.new
  end

  # GET /campos/1/edit
  def edit
  end

  # POST /campos
  # POST /campos.json
  def create
    @campo = Campo.new(campo_params)

    respond_to do |format|
      if @campo.save
        format.html { redirect_to @campo, notice: 'Campo was successfully created.' }
        format.json { render :show, status: :created, location: @campo }
      else
        format.html { render :new }
        format.json { render json: @campo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /campos/1
  # PATCH/PUT /campos/1.json
  def update
    respond_to do |format|
      if @campo.update(campo_params)
        format.html { redirect_to @campo, notice: 'Campo was successfully updated.' }
        format.json { render :show, status: :ok, location: @campo }
      else
        format.html { render :edit }
        format.json { render json: @campo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campos/1
  # DELETE /campos/1.json
  def destroy
    @campo.destroy
    respond_to do |format|
      format.html { redirect_to campos_url, notice: 'Campo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_campo
      @campo = Campo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def campo_params
      params.require(:campo).permit(:layout_id, :number_field, :name, :parent, :level)
    end
end
